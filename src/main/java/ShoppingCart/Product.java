
package ShoppingCart;

/**
 *
 * @author akret
 */
public class Product {
    private String name;
    protected double price;
    
    public Product(String name, double price){
        this.name=name;
        this.price=price;
    }
}
