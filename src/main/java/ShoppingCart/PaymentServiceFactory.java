
package ShoppingCart;

/**
 *
 * @author akret
 */
public class PaymentServiceFactory {
    public static PaymentServiceFactory instance;
    
    private PaymentServiceFactory(){}
    
    public static PaymentServiceFactory getInstance(){
        if(instance==null){
        instance = new PaymentServiceFactory();
        }
        return instance;
    }
    
    public PaymentService getPaymentServiceType(PaymentServiceType type){
        switch(type){
            case CREDIT: return new PaymentService();
            case DEBIT: return new PaymentService();
        }
        return null;
    }
}
