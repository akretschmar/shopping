
package ShoppingCart;

/**
 *
 * @author akret
 */
public class ShoppingCartDemo {

    public static void main(String[] args) {
       PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
       PaymentService creditService = factory.getPaymentServiceType(PaymentServiceType.CREDIT);
       PaymentService debitService = factory.getPaymentServiceType(PaymentServiceType.DEBIT);
       
       Cart cart = new Cart();
       cart.addProduct(new Product("shirt", 50));
       cart.addProduct(new Product("pants", 60));
       
       cart.setPaymentService(creditService);
       cart.payCart();
       
       cart.setPaymentService(debitService);
       cart.payCart();
       
        System.out.println("Processing " + creditService.getClass().getCanonicalName());
    }
    
}
